---
layout: podcast
tag: Podcast
# Required
title: S1E1 - Imposter syndrome
audio_url: /assets/episodes/episode1.mp3
audio_length: "9864553"
audio_type: audio/mpeg
# Recommended
guid: 1
date: 2019-06-28 12:00 -0800
duration: "11:43"
# Situational
season: 1
episode: 1
---

In our pilot episode, we talk about imposter syndrome, which is a common feeling
among students, especially in computer science.

Imposter syndrome is an adverse belief that one's accomplishments are the
product of luck or fraud rather than skill. This belief is unfortunately common
for many students in Computer Science who doubt their talent because they don't
fit the typical cliché of the nerd who has been programming since age eight.
