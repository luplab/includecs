---
layout: podcast
tag: Podcast
# Required
title: S2E1 - Impact of early CS education
audio_url: /assets/episodes/episode5.mp3
audio_length: "21906213"
audio_type: audio/mpeg
# Recommended
guid: 5
date: 2023-06-30 11:56 -0700
duration: "28:41"
# Situational
season: 2
episode: 1
---

Research shows that early CS education may help CS students perform better in
college. In this episode, we explore this question via interviews with college
students and college professors.

Early CS education means that students would take CS classes as part of their
K-12 curriculum and would therefore arrive in college with some prior knowledge.
We talk to Matthew Butner, a lecturer at UC Davis who often teaches intro CS
classes, and ask him what assumptions he makes about students and whether or not
prior CS knowledge is important. We also talk with multiple CS majors, some who
had prior CS experience before going to college and some who didn't.

This episode was produced by Saili Karkare and Joël Porquet-Lupine.
