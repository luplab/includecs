---
layout: podcast
tag: Podcast
# Required
title: S1E2 - Will AI be your best friend tomorrow?
audio_url: /assets/episodes/episode2.mp3
audio_length: "26959536"
audio_type: audio/mpeg
# Recommended
guid: 2
date: 2020-01-31 12:00 -0800
duration: "28:04"
# Situational
season: 1
episode: 2
---

We try to explore the question of whether artificial intelligence will love us
or kill us first, during an interview with UC Davis Professor Zhou Yu.

Professor Yu is an expert in the field of Natural Language Processing (NLP),
leading UC Davis' Team Gunrock to win first place in the 2018 Alexa Socialbot
Grand Challenge. In the interview, we touch on the meaning of artificial
intelligence, diversity and ethics in technology, how Professor Yu won the
challenge, and what is the future of AI.
