---
layout: podcast
tag: Podcast
# Required
title: S1E3 - How secure is election technology?
audio_url: /assets/episodes/episode3.mp3
audio_length: "42394271"
audio_type: audio/mpeg
# Recommended
guid: 3
date: 2020-06-05 12:00 -0800
duration: "29:26"
# Situational
season: 1
episode: 3
---

As the next presidential election is coming ahead, we talk with UC Davis
Professor Matthew Bishop about election technology and its security.

Professor Bishop is a cybersecurity expert who has helped election officials in
California and across the country vet their electronic voting systems, and fix
issues when he finds them.
