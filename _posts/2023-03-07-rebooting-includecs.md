---
layout: post
tag: News
title: "Rebooting #include<cs>"
---

After a couple years on standby due to the pandemic, we are finally rebooting
`#include<cs>`! First, here is a new website, more low-key and hands-on than our
old Wordpress instance. And most of all, the new [team]({% link people.md %}) is
working on new episodes which we are hoping to release by the end of the school
year...  Stay tuned!
