---
layout: podcast
tag: Podcast
# Required
title: S1E4 - Stories from the bunker!
audio_url: /assets/episodes/episode4.mp3
audio_length: "20406692"
audio_type: audio/mpeg
# Recommended
guid: 4
date: 2020-12-14 12:00 -0800
duration: "21:15"
# Situational
season: 1
episode: 4
---

The 2020 COVID lockdown has fundamentally changed people's lives. In this
episode, we talk with students, faculty, and many other people about their
experiences so far.

The interviews were conducted during the Spring of 2020 and asked a wide range
of people (UC Davis Chancellor, Computer Science department chair, CS students,
family and friends) how the lockdown has altered their daily lives.
