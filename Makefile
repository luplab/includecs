all: help

help:
	@echo "help     - Show this help"
	@echo "serve    - Serve website locally"
	@echo "new      - Create new post"

serve:
	bundle exec jekyll serve

new:
	@read -e -p "Post title? " title; \
	bundle exec jekyll post "$$title"
