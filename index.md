---
layout: home
list_title: "Most recent news or podcast episodes"
---

# Welcome!

`#include<cs>` is a **podcast** about computer science and technology with an
extra dose of fun. It uses non-technical language to explain computer science
history, issues, and news.

Subscribe to our podcast episodes on:

- Any podcast client: [podcast.xml]({% link podcast.xml %})
- Spotify: <https://open.spotify.com/show/6P0UmNCX946XxtmYmEAnXx>
- iTunes: <https://podcasts.apple.com/us/podcast/includecs/id1500983175>

Originally created by Janelle Marie Salanga and Joël Porquet-Lupine @ UC Davis.

